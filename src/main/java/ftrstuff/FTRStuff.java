package ftrstuff;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class FTRStuff implements ModInitializer {
	public static final Item PLANT_MATTER = new Item(new FabricItemSettings().group(ItemGroup.MISC).maxCount(32));

	@Override
	public void onInitialize() {
		Registry.register(Registry.ITEM, new Identifier("ftrstuff", "plant_matter"), PLANT_MATTER);
	}

}
